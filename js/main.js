(function ($) {
    "use strict";
    
    
    // Initiate the wowjs
    new WOW().init();


    // // Fixed Navbar
    // $('.fixed-top').css('top', $('.top-bar').height());
    // $(window).scroll(function () {
    //     if ($(this).scrollTop()) {
    //         $('.fixed-top').addClass('bg-dark').css('top', 0);
    //     } else {
    //         $('.fixed-top').removeClass('bg-dark').css('top', $('.top-bar').height());
    //     }
    // });
    
    


    // Header carousel
    $(".header-carousel").owlCarousel({
        autoplay: false,
        smartSpeed: 1500,
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        navText : [
            '<i class="bi bi-chevron-left"></i>',
            '<i class="bi bi-chevron-right"></i>'
        ]
    });


    // Facts counter
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 2000
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: false,
        smartSpeed: 1000,
        margin: 25,
        loop: true,
        center: true,
        dots: false,
        nav: true,
        navText : [
            '<i class="bi bi-chevron-left"></i>',
            '<i class="bi bi-chevron-right"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            }
        }
    });

    
})(jQuery);
let loguser=JSON.parse(localStorage.getItem("loggedUser"))
let user_display=document.getElementById("user_display")
user_display.innerText=loguser.name

function logOut(){
    var result=confirm("Are You Sure You Want to Logout")
    if(result==true){
        location.href="../Login/Login.html"
    }
}